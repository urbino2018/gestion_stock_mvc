/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author ahoad
 *
 */

@Entity
public class Fournisseur implements Serializable {

	@Id
	@GeneratedValue
	private Long idFournisseur;

	private String nom;

	private String prenom;

	private String adresse;

	private String photo;

	private String mail;
	
	@OneToMany(mappedBy="fournisseur")
	private List<CommandeFournisseur> commandeFournisseurs; 
	
	public Fournisseur() {
		super(); 
	}
	/**
	 * @return the id
	 */
	public Long getIdFournisseur() {
		return idFournisseur;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setIdFournisseur(Long id) {
		this.idFournisseur = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the photo
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * @param photo the photo to set
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * @return the commandeFournisseurs
	 */
	public List<CommandeFournisseur> getCommandeFournisseurs() {
		return commandeFournisseurs;
	}
	/**
	 * @param commandeFournisseurs the commandeFournisseurs to set
	 */
	public void setCommandeFournisseurs(List<CommandeFournisseur> commandeFournisseurs) {
		this.commandeFournisseurs = commandeFournisseurs;
	}
	
	

}
