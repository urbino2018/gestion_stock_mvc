/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ahoad
 *
 */

@Entity
public class CommandeClient implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idCommandeClient;
	
	private String code; 
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommande; 
	
	@ManyToOne
	@JoinColumn(name="idClient")
	private Client client; 
	
	@OneToMany(mappedBy="commandeClient")
	private List<LigneCommandeClient> ligneCommandeClients; 
	
	

	/**
	 * @return the id
	 */
	public Long getIdCommandeClient() {
		return idCommandeClient;
	}

	/**
	 * @param id the id to set
	 */
	public void setIdCommandeClient(Long id) {
		this.idCommandeClient = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the dateCommande
	 */
	public Date getDateCommande() {
		return dateCommande;
	}

	/**
	 * @param dateCommande the dateCommande to set
	 */
	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	/**
	 * @return the clients
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @param clients the clients to set
	 */
	public void setClient(Client clients) {
		this.client = clients;
	}

	/**
	 * @return the ligneCommandeClients
	 */
	public List<LigneCommandeClient> getLigneCommandeClients() {
		return ligneCommandeClients;
	}

	/**
	 * @param ligneCommandeClients the ligneCommandeClients to set
	 */
	public void setLigneCommandeClients(List<LigneCommandeClient> ligneCommandeClients) {
		this.ligneCommandeClients = ligneCommandeClients;
	} 
	
}
