/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author ahoad
 *
 */

@Entity
public class LigneCommandeClient implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idLigneCdeClt;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article arcticle; 
	
	@ManyToOne
	@JoinColumn(name="idCommandeClient")
	private CommandeClient commandeClient; 

	/**
	 * @return the id
	 */
	public Long getIdLigneCdeClt() {
		return idLigneCdeClt;
	}

	/**
	 * @param id the id to set
	 */
	public void setIdLigneCdeClt(Long id) {
		this.idLigneCdeClt = id;
	}

	/**
	 * @return the arcticle
	 */
	public Article getArcticle() {
		return arcticle;
	}

	/**
	 * @param arcticle the arcticle to set
	 */
	public void setArcticle(Article arcticle) {
		this.arcticle = arcticle;
	}

	/**
	 * @return the commandeClient
	 */
	public CommandeClient getCommandeClient() {
		return commandeClient;
	}

	/**
	 * @param commandeClient the commandeClient to set
	 */
	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	} 
	
}
