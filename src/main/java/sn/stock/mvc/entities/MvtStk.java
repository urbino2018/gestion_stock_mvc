/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ahoad
 *
 */

@Entity
public class MvtStk implements Serializable{
	
	public static final int ENTREE =1; 
	
	public static final int SORTIE = 2; 
	
	@Id
	@GeneratedValue
	private Long idMvtStk;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvtStk; 
	
	private BigDecimal quantite; 
	
	private int typeMvt; 
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article; 

	/**
	 * @return the id
	 */
	public Long getIdMvtStk() {
		return idMvtStk;
	}

	/**
	 * @param id the id to set
	 */
	public void setIdMvtStk(Long id) {
		this.idMvtStk = id;
	}

	/**
	 * @return the dateMvtStk
	 */
	public Date getDateMvtStk() {
		return dateMvtStk;
	}

	/**
	 * @param dateMvtStk the dateMvtStk to set
	 */
	public void setDateMvtStk(Date dateMvtStk) {
		this.dateMvtStk = dateMvtStk;
	}

	/**
	 * @return the quantite
	 */
	public BigDecimal getQuantite() {
		return quantite;
	}

	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	/**
	 * @return the typeMvt
	 */
	public int getTypeMvt() {
		return typeMvt;
	}

	/**
	 * @param typeMvt the typeMvt to set
	 */
	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}

	/**
	 * @return the article
	 */
	public Article getArticle() {
		return article;
	}

	/**
	 * @param article the article to set
	 */
	public void setArticle(Article article) {
		this.article = article;
	} 
	
}
