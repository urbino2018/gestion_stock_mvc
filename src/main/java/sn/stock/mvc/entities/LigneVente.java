/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author ahoad
 *
 */

@Entity
public class LigneVente implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idLigneVente;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article; 
	
	@ManyToOne
	@JoinColumn(name="vente")
	private Vente vente; 

	/**
	 * @return the id
	 */
	public Long getIdLigneVente() {
		return idLigneVente;
	}

	/**
	 * @param id the id to set
	 */
	public void setIdLigneVente(Long id) {
		this.idLigneVente = id;
	}

	/**
	 * @return the article
	 */
	public Article getArticle() {
		return article;
	}

	/**
	 * @param article the article to set
	 */
	public void setArticle(Article article) {
		this.article = article;
	}

	/**
	 * @return the vente
	 */
	public Vente getVente() {
		return vente;
	}

	/**
	 * @param vente the vente to set
	 */
	public void setVente(Vente vente) {
		this.vente = vente;
	} 
	
}
