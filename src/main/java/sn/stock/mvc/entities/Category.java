/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author ahoad
 *
 */

@Entity
public class Category implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idCategory; 
	
	private String code; 
	
	private String designation; 
	
	@OneToMany(mappedBy="category")
	private List<Article> articles; 
	
	
	public Category() {
	}

	/**
	 * @return the id
	 */
	public Long getIdCategory() {
		return idCategory;
	}

	/**
	 * @param id the id to set
	 */
	public void setIdCategory(Long id) {
		this.idCategory = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the articles
	 */
	public List<Article> getArticles() {
		return articles;
	}

	/**
	 * @param articles the articles to set
	 */
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	} 
	
}
