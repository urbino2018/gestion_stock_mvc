/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author ahoad
 *
 */

@Entity
public class LigneCommandeFournisseur implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idLigneCdeFrs;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article arcticle; 
	
	@ManyToOne
	@JoinColumn(name="idCommandeFournisseur")
	private CommandeFournisseur commandeFournisseur;

	/**
	 * @return the id
	 */
	public Long getIdLigneCdeFrs() {
		return idLigneCdeFrs;
	}

	/**
	 * @param id the id to set
	 */
	public void setIdLigneCdeFrs(Long id) {
		this.idLigneCdeFrs = id;
	}

	/**
	 * @return the arcticle
	 */
	public Article getArcticle() {
		return arcticle;
	}

	/**
	 * @param arcticle the arcticle to set
	 */
	public void setArcticle(Article arcticle) {
		this.arcticle = arcticle;
	}

	/**
	 * @return the commandeFournisseur
	 */
	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	/**
	 * @param commandeFournisseur the commandeFournisseur to set
	 */
	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	} 
	
}
