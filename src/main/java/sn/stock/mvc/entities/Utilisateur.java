/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author ahoad
 *
 */

@Entity
public class Utilisateur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idUtilisateur;
	
	private String nom; 
	
	private String prenom; 
	
	private String mail;
	
	private String motDePasse; 
	
	private String photo; 

	/**
	 * @return the id
	 */
	public Long getIdUtilisateur() {
		return idUtilisateur;
	}

	/**
	 * @param id the id to set
	 */
	public void setIdUtilisateur(Long id) {
		this.idUtilisateur = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the motDePasse
	 */
	public String getMotDePasse() {
		return motDePasse;
	}

	/**
	 * @param motDePasse the motDePasse to set
	 */
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	/**
	 * @return the photo
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * @param photo the photo to set
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	} 
	
}
