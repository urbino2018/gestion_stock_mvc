/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author ahoad
 *
 */
@Entity
@Table(name = "article")
public class Article implements Serializable {
	// Declaration des attributs de la classe

	@Id
	@GeneratedValue
	private Long idArticle;

	private String codeArticle;

	private String designation;

	private BigDecimal prixUnitaireHT;

	private BigDecimal tauxTva;

	private BigDecimal prixUnitaireTTC;

	private String photo;
	
	@ManyToOne
	@JoinColumn(name="idCategory")
	private Category category;
	
	public Article() {
	}

	/**
	 * @return the idArticle
	 */
	public Long getIdArticle() {
		return idArticle;
	}

	/**
	 * @param idArticle
	 *            the idArticle to set
	 */
	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}

	/**
	 * @return the codeArticle
	 */
	public String getCodeArticle() {
		return codeArticle;
	}

	/**
	 * @param codeArticle
	 *            the codeArticle to set
	 */
	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation
	 *            the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the prixUnitaireHT
	 */
	public BigDecimal getPrixUnitaireHT() {
		return prixUnitaireHT;
	}

	/**
	 * @param prixUnitaireHT
	 *            the prixUnitaireHT to set
	 */
	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		this.prixUnitaireHT = prixUnitaireHT;
	}

	/**
	 * @return the tauxTva
	 */
	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	/**
	 * @param tauxTva
	 *            the tauxTva to set
	 */
	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	/**
	 * @return the prixUnitaireTTC
	 */
	public BigDecimal getPrixUnitaireTTC() {
		return prixUnitaireTTC;
	}

	/**
	 * @param prixUnitaireTTC
	 *            the prixUnitaireTTC to set
	 */
	public void setPrixUnitaireTTC(BigDecimal prixUnitaireTTC) {
		this.prixUnitaireTTC = prixUnitaireTTC;
	}

	/**
	 * @return the photo
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * @param photo
	 *            the photo to set
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

}
