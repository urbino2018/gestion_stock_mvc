/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ahoad
 *
 */

@Entity
public class Vente implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idVente;
	
	private String code; 
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente; 
	
	@OneToMany(mappedBy="vente")
	private List<LigneVente> ligneVentes; 
	/**
	 * @return the id
	 */
	public Long getIdVente() {
		return idVente;
	}

	/**
	 * @param id the id to set
	 */
	public void setIdVente(Long id) {
		this.idVente = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the dateVente
	 */
	public Date getDateVente() {
		return dateVente;
	}

	/**
	 * @param dateVente the dateVente to set
	 */
	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}

	/**
	 * @return the ligneVentes
	 */
	public List<LigneVente> getLigneVentes() {
		return ligneVentes;
	}

	/**
	 * @param ligneVentes the ligneVentes to set
	 */
	public void setLigneVentes(List<LigneVente> ligneVentes) {
		this.ligneVentes = ligneVentes;
	} 
	
}
