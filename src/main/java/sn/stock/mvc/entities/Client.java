/**
 * 
 */
package sn.stock.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author ahoad
 *
 */

@Entity
public class Client implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idClient;
	
	private String nom; 
	
	private String prenom; 
	
	private String adresse; 
	
	private String photo; 
	
	private String mail; 
	
	@OneToMany(mappedBy="client")
	private List<CommandeClient> commandeClients; 
	
	public Client() {
	}

	/**
	 * @return the id
	 */
	public Long getIdClient() {
		return idClient;
	}

	/**
	 * @param id the id to set
	 */
	public void setIdClient(Long id) {
		this.idClient = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the photo
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * @param photo the photo to set
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the commandeClients
	 */
	public List<CommandeClient> getCommandeClients() {
		return commandeClients;
	}

	/**
	 * @param commandeClients the commandeClients to set
	 */
	public void setCommandeClients(List<CommandeClient> commandeClients) {
		this.commandeClients = commandeClients;
	} 
	
}
