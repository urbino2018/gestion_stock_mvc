/**
 * 
 */
package sn.stock.mvc;

import java.io.File;
import java.io.FileInputStream;

import java.io.InputStream;

import sn.stock.mvc.dao.impl.FlickrDaoImpl;

/**
 * @author ahoad
 *
 */
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FlickrDaoImpl flickr = new FlickrDaoImpl();
		// flickr.auth();

		try {
			InputStream stream = new FileInputStream(new File("E:\\flickr_test.jpg"));
			String url = flickr.savePhoto(stream, "monImage");
			System.out.println(url);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}
