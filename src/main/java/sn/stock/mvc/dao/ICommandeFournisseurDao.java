/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.CommandeFournisseur;

/**
 * @author ahoad
 *
 */
public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur> {

}
