/**
 * 
 */
package sn.stock.mvc.dao;

import java.io.InputStream;

/**
 * @author ahoad
 *
 */
public interface IFlickrDao {
	// Savoir-Faire permettant une photo sur un serveur distant
		public String savePhoto( InputStream photo, String title)throws Exception;
}
