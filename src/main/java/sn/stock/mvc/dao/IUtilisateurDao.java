/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.Utilisateur;

/**
 * @author ahoad
 *
 */
public interface IUtilisateurDao extends IGenericDao<Utilisateur>{

}
