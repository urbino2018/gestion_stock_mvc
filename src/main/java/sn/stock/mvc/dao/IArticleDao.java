/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.Article;

/**
 * @author ahoad
 *
 */
public interface IArticleDao extends IGenericDao<Article> {

}
