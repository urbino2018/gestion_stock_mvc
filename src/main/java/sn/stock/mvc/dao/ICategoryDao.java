/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.Category;

/**
 * @author ahoad
 *
 */
public interface ICategoryDao extends IGenericDao<Category>{

}
