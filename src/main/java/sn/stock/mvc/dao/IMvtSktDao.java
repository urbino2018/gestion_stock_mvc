/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.MvtStk;

/**
 * @author ahoad
 *
 */
public interface IMvtSktDao extends IGenericDao<MvtStk> {

}
