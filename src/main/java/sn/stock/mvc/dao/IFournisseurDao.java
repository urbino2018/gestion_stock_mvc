/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.Fournisseur;

/**
 * @author ahoad
 *
 */
public interface IFournisseurDao extends IGenericDao<Fournisseur>{

}
