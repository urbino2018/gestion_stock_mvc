/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.Client;

/**
 * @author ahoad
 *
 */
public interface IClientDao extends IGenericDao<Client>{

}
