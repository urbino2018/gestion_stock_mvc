/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.CommandeClient;

/**
 * @author ahoad
 *
 */
public interface ICommandeClientDao extends IGenericDao<CommandeClient> {

}
