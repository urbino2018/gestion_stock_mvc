/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.LigneCommandeFournisseur;

/**
 * @author ahoad
 *
 */
public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur> {

}
