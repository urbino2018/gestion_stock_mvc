/**
 * 
 */
package sn.stock.mvc.dao;

import java.util.List;

/**
 * @author ahoad
 *
 */

public interface IGenericDao<E> {
	
	// Declaration des methodes generiques de notre application de gestion de stock
	
	public E save(E entity); 
	
	public E update(E entity); 
	
	public List<E> selectAll(); 
	
	public List<E> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
		
	public E getById( Long id); 
	
	public void remove(Long id); 
	
	public E findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
	
	public E findOne( String[] paramNames, Object[] paramValues);
	
	public int findCountBy( String paramName, String paramValue); 
}
