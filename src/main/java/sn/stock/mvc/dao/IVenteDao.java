/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.Vente;

/**
 * @author ahoad
 *
 */
public interface IVenteDao extends IGenericDao<Vente> {

}
