/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.LigneCommandeClient;

/**
 * @author ahoad
 *
 */
public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient> {

}
