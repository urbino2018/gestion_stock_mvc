/**
 * 
 */
package sn.stock.mvc.dao;

import sn.stock.mvc.entities.LigneVente;

/**
 * @author ahoad
 *
 */
public interface ILigneVenteDao extends IGenericDao<LigneVente> {

}
