/**
 * 
 */
package sn.stock.mvc.dao.impl;

import sn.stock.mvc.dao.IUtilisateurDao;
import sn.stock.mvc.entities.Utilisateur;

/**
 * @author ahoad
 *
 */
public class UtilisateurDaoImpl extends GenericDaoImpl<Utilisateur> implements IUtilisateurDao{

}
