/**
 * 
 */
package sn.stock.mvc.dao.impl;

import sn.stock.mvc.dao.IClientDao;
import sn.stock.mvc.entities.Client;

/**
 * @author ahoad
 *
 */
public class ClientDaoImpl extends GenericDaoImpl<Client> implements IClientDao{

	
}
