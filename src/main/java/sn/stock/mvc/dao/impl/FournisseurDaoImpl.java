/**
 * 
 */
package sn.stock.mvc.dao.impl;

import sn.stock.mvc.dao.IFournisseurDao;
import sn.stock.mvc.entities.Fournisseur;

/**
 * @author ahoad
 *
 */
public class FournisseurDaoImpl extends GenericDaoImpl<Fournisseur> implements IFournisseurDao{
	
}
