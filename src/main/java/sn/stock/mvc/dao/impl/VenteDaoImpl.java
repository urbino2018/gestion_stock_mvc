/**
 * 
 */
package sn.stock.mvc.dao.impl;

import sn.stock.mvc.dao.IVenteDao;
import sn.stock.mvc.entities.Vente;

/**
 * @author ahoad
 *
 */
public class VenteDaoImpl extends GenericDaoImpl<Vente> implements IVenteDao{

}
