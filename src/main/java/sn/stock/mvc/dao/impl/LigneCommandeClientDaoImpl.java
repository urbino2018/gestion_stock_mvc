/**
 * 
 */
package sn.stock.mvc.dao.impl;

import sn.stock.mvc.dao.ILigneCommandeClientDao;
import sn.stock.mvc.entities.LigneCommandeClient;

/**
 * @author ahoad
 *
 */
public class LigneCommandeClientDaoImpl extends GenericDaoImpl<LigneCommandeClient> implements ILigneCommandeClientDao{

}
