/**
 * 
 */
package sn.stock.mvc.dao.impl;

import sn.stock.mvc.dao.IArticleDao;
import sn.stock.mvc.entities.Article;

/**
 * @author ahoad
 *
 */
public class ArticleDaoImpl extends GenericDaoImpl<Article> implements IArticleDao {


}
