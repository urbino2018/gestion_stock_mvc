/**
 * 
 */
package sn.stock.mvc.dao.impl;

import sn.stock.mvc.dao.ILigneCommandeFournisseurDao;
import sn.stock.mvc.entities.LigneCommandeFournisseur;

/**
 * @author ahoad
 *
 */
public class LigneCommandeFournisseurDaoImpl extends GenericDaoImpl<LigneCommandeFournisseur> implements ILigneCommandeFournisseurDao{


}
