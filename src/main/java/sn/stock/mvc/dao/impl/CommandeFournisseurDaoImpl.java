/**
 * 
 */
package sn.stock.mvc.dao.impl;

import sn.stock.mvc.dao.ICommandeFournisseurDao;
import sn.stock.mvc.entities.CommandeFournisseur;

/**
 * @author ahoad
 *
 */
public class CommandeFournisseurDaoImpl extends GenericDaoImpl<CommandeFournisseur> implements ICommandeFournisseurDao{

}
