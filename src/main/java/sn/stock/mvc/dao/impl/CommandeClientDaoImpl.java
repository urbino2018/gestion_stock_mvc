/**
 * 
 */
package sn.stock.mvc.dao.impl;

import sn.stock.mvc.dao.ICommandeClientDao;
import sn.stock.mvc.entities.CommandeClient;

/**
 * @author ahoad
 *
 */
public class CommandeClientDaoImpl extends GenericDaoImpl<CommandeClient> implements ICommandeClientDao{


}
