/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.CommandeFournisseur;

/**
 * @author ahoad
 *
 */
public interface ICommandeFournisseurService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public CommandeFournisseur save(CommandeFournisseur entity); 
		
		public CommandeFournisseur update(CommandeFournisseur entity); 
		
		public List<CommandeFournisseur> selectAll(); 
		
		public List<CommandeFournisseur> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public CommandeFournisseur getById( Long id); 
		
		public void remove(Long id); 
		
		public CommandeFournisseur findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public CommandeFournisseur findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
