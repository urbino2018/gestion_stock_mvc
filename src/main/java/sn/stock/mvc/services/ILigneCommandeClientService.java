/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.LigneCommandeClient;

/**
 * @author ahoad
 *
 */
public interface ILigneCommandeClientService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public LigneCommandeClient save(LigneCommandeClient entity); 
		
		public LigneCommandeClient update(LigneCommandeClient entity); 
		
		public List<LigneCommandeClient> selectAll(); 
		
		public List<LigneCommandeClient> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public LigneCommandeClient getById( Long id); 
		
		public void remove(Long id); 
		
		public LigneCommandeClient findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public LigneCommandeClient findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
