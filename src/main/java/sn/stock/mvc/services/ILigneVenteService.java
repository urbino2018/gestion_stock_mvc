/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.LigneVente;

/**
 * @author ahoad
 *
 */
public interface ILigneVenteService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public LigneVente save(LigneVente entity); 
		
		public LigneVente update(LigneVente entity); 
		
		public List<LigneVente> selectAll(); 
		
		public List<LigneVente> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public LigneVente getById( Long id); 
		
		public void remove(Long id); 
		
		public LigneVente findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public LigneVente findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
