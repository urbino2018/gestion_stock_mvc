/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.CommandeClient;

/**
 * @author ahoad
 *
 */
public interface ICommandeClientService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public CommandeClient save(CommandeClient entity); 
		
		public CommandeClient update(CommandeClient entity); 
		
		public List<CommandeClient> selectAll(); 
		
		public List<CommandeClient> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public CommandeClient getById( Long id); 
		
		public void remove(Long id); 
		
		public CommandeClient findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public CommandeClient findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
