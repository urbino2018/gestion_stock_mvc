/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.Article;

/**
 * @author ahoad
 *
 */
public interface IArticleService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public Article save(Article entity); 
		
		public Article update(Article entity); 
		
		public List<Article> selectAll(); 
		
		public List<Article> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public Article getById( Long id); 
		
		public void remove(Long id); 
		
		public Article findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public Article findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
