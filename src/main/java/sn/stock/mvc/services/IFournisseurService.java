/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.Fournisseur;

/**
 * @author ahoad
 *
 */
public interface IFournisseurService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public Fournisseur save(Fournisseur entity); 
		
		public Fournisseur update(Fournisseur entity); 
		
		public List<Fournisseur> selectAll(); 
		
		public List<Fournisseur> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public Fournisseur getById( Long id); 
		
		public void remove(Long id); 
		
		public Fournisseur findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public Fournisseur findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
