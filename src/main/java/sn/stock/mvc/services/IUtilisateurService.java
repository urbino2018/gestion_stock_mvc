/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.Utilisateur;

/**
 * @author ahoad
 *
 */
public interface IUtilisateurService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public Utilisateur save(Utilisateur entity); 
		
		public Utilisateur update(Utilisateur entity); 
		
		public List<Utilisateur> selectAll(); 
		
		public List<Utilisateur> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public Utilisateur getById( Long id); 
		
		public void remove(Long id); 
		
		public Utilisateur findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public Utilisateur findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
