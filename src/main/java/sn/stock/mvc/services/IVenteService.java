/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.Vente;

/**
 * @author ahoad
 *
 */
public interface IVenteService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public Vente save(Vente entity); 
		
		public Vente update(Vente entity); 
		
		public List<Vente> selectAll(); 
		
		public List<Vente> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public Vente getById( Long id); 
		
		public void remove(Long id); 
		
		public Vente findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public Vente findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
