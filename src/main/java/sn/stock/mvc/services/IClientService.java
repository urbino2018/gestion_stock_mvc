/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.Client;

/**
 * @author ahoad
 *
 */
public interface IClientService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public Client save(Client entity); 
		
		public Client update(Client entity); 
		
		public List<Client> selectAll(); 
		
		public List<Client> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public Client getById( Long id); 
		
		public void remove(Long id); 
		
		public Client findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public Client findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
