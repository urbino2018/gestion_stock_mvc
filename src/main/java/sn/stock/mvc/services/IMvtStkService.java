/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.MvtStk;

/**
 * @author ahoad
 *
 */
public interface IMvtStkService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public MvtStk save(MvtStk entity); 
		
		public MvtStk update(MvtStk entity); 
		
		public List<MvtStk> selectAll(); 
		
		public List<MvtStk> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public MvtStk getById( Long id); 
		
		public void remove(Long id); 
		
		public MvtStk findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public MvtStk findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
