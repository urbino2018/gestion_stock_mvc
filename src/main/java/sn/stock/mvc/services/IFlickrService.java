/**
 * 
 */
package sn.stock.mvc.services;

import java.io.InputStream;

/**
 * @author ahoad
 *
 */
public interface IFlickrService {
	// Savoir-Faire permettant une photo sur un serveur distant
			public String savePhoto( InputStream photo, String title)throws Exception;
}
