/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.LigneCommandeFournisseur;

/**
 * @author ahoad
 *
 */
public interface ILigneCommandeFournisseurService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public LigneCommandeFournisseur save(LigneCommandeFournisseur entity); 
		
		public LigneCommandeFournisseur update(LigneCommandeFournisseur entity); 
		
		public List<LigneCommandeFournisseur> selectAll(); 
		
		public List<LigneCommandeFournisseur> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public LigneCommandeFournisseur getById( Long id); 
		
		public void remove(Long id); 
		
		public LigneCommandeFournisseur findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public LigneCommandeFournisseur findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
