/**
 * 
 */
package sn.stock.mvc.services.impl;

import java.io.InputStream;

import sn.stock.mvc.dao.IFlickrDao;
import sn.stock.mvc.services.IFlickrService;

/**
 * @author ahoad
 *
 */
public class FlickrServiceImpl implements IFlickrService{
	private IFlickrDao dao ; 
	public void setDao(IFlickrDao dao) {
		this.dao = dao;
	}
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
	
		return dao.savePhoto(photo, title);
	}

}
