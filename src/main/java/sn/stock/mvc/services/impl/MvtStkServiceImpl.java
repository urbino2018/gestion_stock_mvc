/**
 * 
 */
package sn.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import sn.stock.mvc.dao.IMvtSktDao;
import sn.stock.mvc.entities.MvtStk;
import sn.stock.mvc.services.IMvtStkService;

/**
 * @author ahoad
 *
 */

@Transactional
public class MvtStkServiceImpl implements IMvtStkService{

	private IMvtSktDao dao; 
	
	// Methode me permettant de faire d'injection des dependances
	public void setDao(IMvtSktDao dao) {
		this.dao = dao;
	}
	@Override
	public MvtStk save(MvtStk entity) {
		return dao.save(entity);
	}

	@Override
	public MvtStk update(MvtStk entity) {
		return dao.update(entity);
	}

	@Override
	public List<MvtStk> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<MvtStk> selectAll(String sortieField, String sort) {
		return dao.selectAll(sortieField, sort);
	}

	@Override
	public MvtStk getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);		
	}

	@Override
	public MvtStk findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public MvtStk findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
