/**
 * 
 */
package sn.stock.mvc.services;

import java.util.List;

import sn.stock.mvc.entities.Category;

/**
 * @author ahoad
 *
 */
public interface ICategoryService {
	// Declaration des methodes generiques de notre application de gestion de stock
	
		public Category save(Category entity); 
		
		public Category update(Category entity); 
		
		public List<Category> selectAll(); 
		
		public List<Category> selectAll( String sortieField, String sort ); // Methode permettant de faire le tri. 
			
		public Category getById( Long id); 
		
		public void remove(Long id); 
		
		public Category findOne( String paramName, Object paramValue); // Methode permettant de recuperer un object en passant par le nom de sa valeur. 
		
		public Category findOne( String[] paramNames, Object[] paramValues);
		
		public int findCountBy( String paramName, String paramValue);	

}
